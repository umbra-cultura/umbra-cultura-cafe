const CACHE_NAME = 'V1';
const STATIC_CACHE_URLS = ['/', '/css/index.css', '/css/prism-diff.css', '/css/prism-okaidia.css'];

self.addEventListener('install', event => {
  console.log('Instalando o Service Worker.');
	event.waitUntil(
    caches.open(CACHE_NAME)
    .then(cache => cache.addAll(STATIC_CACHE_URLS))
	);
});

self.addEventListener('fetch', event => {
  // Cache-First Strategy
  event.respondWith(
    caches.match(event.request) // check if the request has already been cached
    .then(cached => cached || fetch(event.request)) // otherwise request network
  );
});

self.addEventListener('activate', event => {
  console.log('Ativando o Service Worker.');

	event.waitUntil(
    caches.keys()
    .then(keys => keys.filter(key => key !== CACHE_NAME))
    .then(keys => Promise.all(keys.map(key => {
        console.log(`Deleting cache ${key}`);
        return caches.delete(key)
    })))
  );
});
