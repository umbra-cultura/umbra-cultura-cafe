module.exports = {
	title: "Umbra Cultura Café",
	url: "https://umbracultura.cafe/",
	language: "pt-BR",
	description: "I am writing about my experiences as a naval navel-gazer.",
	author: {
		name: "F. S. Lawliet",
		email: "fslawliet@gmail.com",
		url: "https://umbracultura.cafe/about-me/"
	}
}
